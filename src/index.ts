import { HElement } from "peryl/dist/hsml";
import { HApp, HDispatcher, HState, HView } from "peryl/dist/hsml-app";
import { GameScore, Score, Scoreboard } from "./services/scoreboard";

interface State {
    summary: GameScore[];
    game?: {
        home: string;
        away: string;
    },
    gameMessage?: string;
    score?: {
        home: string;
        away: string;
        score: Score;
    }
}

const state: HState<State> = function () {
    return {
        summary: []
    };
};

type Actions =
    | "game-edit"
    | "game-start"
    | "score-edit"
    | "score-update"
    | "score-home-inc"
    | "score-away-inc"
    | "game-finish";

const view: HView<State, Actions> = function (state) {
    return [
        ["div.w3-content", [
            ["h1", "Scoreboard"],
            !(state.game || state.score)
                ? ["div.w3-responsive.w3-card", [
                    ["table.w3-table-all.w3-hoverable", [
                        ["thead", [
                            ["tr", [
                                ["th", "Home team"],
                                ["th", "Away team"],
                                ["th", "Score"],
                                ["th", [
                                    ["a.w3-text-blue",
                                        {
                                            href: "javascript:void(0)",
                                            on: ["click", "game-edit"],
                                            title: "Start game"
                                        },
                                        [["i.fa.fa-plus"]]
                                    ]
                                ]]
                            ]]
                        ]],
                        ["tbody",
                            state.summary.map<HElement<Actions>>(game =>
                                ["tr", [
                                    ["td", [
                                        ["a.w3-text-green",
                                            {
                                                href: "javascript:void(0)",
                                                on: ["click", "score-home-inc", game],
                                                title: "Gooool!"
                                            },
                                            [["i.fa.fa-futbol-o"]]
                                        ],
                                        " ",
                                        game.home
                                    ]],
                                    ["td", [
                                        ["a.w3-text-green",
                                            {
                                                href: "javascript:void(0)",
                                                on: ["click", "score-away-inc", game],
                                                title: "Gooool!"
                                            },
                                            [["i.fa.fa-futbol-o"]]
                                        ],
                                        " ",
                                        game.away,
                                    ]],
                                    ["td", [
                                        ["a.w3-text-green",
                                            {
                                                href: "javascript:void(0)",
                                                on: ["click", "score-edit", game],
                                                title: "Edit score"
                                            },
                                            [["i.fa.fa-edit"]]
                                        ],
                                        " ",
                                        `${game.score[0]} - ${game.score[1]}`,
                                        " ",
                                        ["span.w3-text-gray.w3-small",
                                            game.score[0] + game.score[1]
                                        ]
                                    ]],
                                    ["td", [
                                        ["a.w3-text-red",
                                            {
                                                href: "javascript:void(0)",
                                                on: ["click", "game-finish", game],
                                                title: "Finish game"
                                            },
                                            [["i.fa.fa-trash"]]
                                        ]
                                    ]]
                                ]])
                        ]
                    ]]
                ]]
                : null,
            state.game
                ? ["div", [
                    ["h2", "Start game"],
                    ["form.w3-container", { on: ["submit", "game-start"] }, [
                        ["p", [
                            ["label", ["Home team",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "home",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 30,
                                    value: state.game!.home
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["label", ["Away team",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "away",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 30,
                                    value: state.game!.away
                                }]
                            ]]
                        ]],
                        state.gameMessage
                            ? ["p.w3-text-red", state.gameMessage]
                            : undefined,
                        ["p", [
                            ["button.w3-btn.w3-blue", "Start"]
                        ]]
                    ]]
                ]]
                : null,
            state.score
                ? ["div", [
                    ["h2", ["Game score: ", state.score!.home, " - ", state.score!.away]],
                    ["form.w3-container", { on: ["submit", "score-update"] }, [
                        ["input", { type: "hidden", name: "home", value: state.score.home }],
                        ["input", { type: "hidden", name: "away", value: state.score.away }],
                        ["p", [
                            ["label", [["b", state.score!.home], " (home)",
                                ["input.w3-input", {
                                    type: "number",
                                    name: "scoreHome",
                                    required: true,
                                    min: 0,
                                    max: 30,
                                    value: state.score!.score[0]
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["label", [["b", state.score!.away], " (away)",
                                ["input.w3-input", {
                                    type: "number",
                                    name: "scoreAway",
                                    required: true,
                                    min: 0,
                                    max: 30,
                                    value: state.score!.score[1]
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["button.w3-btn.w3-blue", "Update"]
                        ]]
                    ]]
                ]]
                : null
        ]]
    ];
};

const dispatcher: HDispatcher<State, Actions> = async function (action, state) {
    console.log(action);

    switch (action.type) {

        case "init":
            state.summary = scoreboard.summary();
            break;

        case "game-edit":
            state.game = { home: "", away: "" };
            state.gameMessage = undefined;
            break;

        case "game-start":
            try {
                scoreboard.startGame(action.data.home, action.data.away);
                state.summary = scoreboard.summary();
                state.game = undefined;
            } catch (error) {
                state.gameMessage = error.message;
            }
            break;

        case "game-finish":
            scoreboard.finishGame(action.data.home, action.data.away);
            state.summary = scoreboard.summary();
            break;

        case "score-edit":
            state.score = action.data;
            break;

        case "score-update":
            scoreboard.updateGameScore(action.data.home, action.data.away,
                [Number(action.data.scoreHome), Number(action.data.scoreAway)]);
            state.summary = scoreboard.summary();
            state.score = undefined;
            break;

        case "score-home-inc":
            scoreboard.updateGameScore(action.data.home, action.data.away,
                [action.data.score[0] + 1, action.data.score[1]]);
            state.summary = scoreboard.summary();
            break;

        case "score-away-inc":
            scoreboard.updateGameScore(action.data.home, action.data.away,
                [action.data.score[0], action.data.score[1] + 1]);
            state.summary = scoreboard.summary();
            break;
    }
};

const scoreboard = new Scoreboard();
const matches: GameScore[] = [
    { home: "Mexico", away: "Canada", score: [0, 5] },
    { home: "Spain", away: "Brazil", score: [10, 2] },
    { home: "Germany", away: "France", score: [2, 2] },
    { home: "Uruguay", away: "Italy", score: [6, 6] },
    { home: "Argentina", away: "Australia", score: [3, 1] }
];
matches.forEach(m => scoreboard.startGame(m.home, m.away));
matches.forEach(m => scoreboard.updateGameScore(m.home, m.away, m.score));


HApp.debug = window.location.hostname === "localhost";

new HApp<State, Actions>(state, view, dispatcher, "app");
