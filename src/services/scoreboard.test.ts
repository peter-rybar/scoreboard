import { expect } from "chai";
import { it } from "mocha";
import { GameScore, Scoreboard } from "./scoreboard";

describe("Scoreboard", function () {

    it("Create scoreboard", function () {
        const scoreboard = new Scoreboard();
        expect(scoreboard).deep.equal({ _gameScores: [] });
    });

    it("Start game", function () {
        // 1. Start a new game, assuming initial score 0 – 0 and adding it the scoreboard.
        // This should capture following parameters:
        // a. Home team
        // b. Away team
        const scoreboard = new Scoreboard();
        scoreboard.startGame("Home", "Away");
        expect(scoreboard).deep.equal({
            _gameScores: [{
                home: "Home",
                away: "Away",
                score: [0, 0]
            }]
        });
        expect(
            () => scoreboard.startGame("Home", "Away")
        ).to.throw("Game aleready exists");
    });

    it("Update game score", function () {
        // 2. Update score. This should receive a pair of absolute scores: home team score and away
        // team score.
        const scoreboard = new Scoreboard();
        scoreboard.startGame("Home", "Away");
        scoreboard.updateGameScore("Home", "Away", [2, 3]);
        expect(scoreboard).deep.equal({
            _gameScores: [{
                home: "Home",
                away: "Away",
                score: [2, 3]
            }]
        });
        expect(
            () => scoreboard.updateGameScore("Home", "Wrong", [0, 0])
        ).to.throw("No such game");
    });

    it("Finish game", function () {
        // 3. Finish game currently in progress. This removes a match from the scoreboard.
        const scoreboard = new Scoreboard();
        scoreboard.startGame("Home", "Away");
        expect(scoreboard).deep.equal({ _gameScores: [{
            home: "Home",
            away: "Away",
            score: [0, 0]
        }] });
        scoreboard.finishGame("Home", "Away");
        expect(scoreboard).deep.equal({ _gameScores: [] });
        expect(
            () => scoreboard.finishGame("Home", "Wrong")
        ).to.throw("No such game");
    });

    it("Summary", function () {
        // 4. Get a summary of games in progress ordered by their total score. The games with the same
        // total score will be returned ordered by the most recently started match in the scoreboard.
        const scoreboard = new Scoreboard();
        const matches: GameScore[] = [
            { home: "Mexico", away: "Canada", score: [0, 5] },
            { home: "Spain", away: "Brazil", score: [10, 2] },
            { home: "Germany", away: "France", score: [2, 2] },
            { home: "Uruguay", away: "Italy", score: [6, 6] },
            { home: "Argentina", away: "Australia", score: [3, 1] }
        ];
        matches.forEach(m => scoreboard.startGame(m.home, m.away));
        matches.forEach(m => scoreboard.updateGameScore(m.home, m.away, m.score));
        const summary = scoreboard.summary();
        // The summary should be as follows:
        // 1. Uruguay 6 - Italy 6
        // 2. Spain 10 - Brazil 2
        // 3. Mexico 0 - Canada 5
        // 4. Argentina 3 - Australia 1
        // 5. Germany 2 - France 2
        expect(summary).deep.equal([
            { home: "Uruguay", away: "Italy", score: [6, 6] },
            { home: "Spain", away: "Brazil", score: [10, 2] },
            { home: "Mexico", away: "Canada", score: [0, 5] },
            { home: "Argentina", away: "Australia", score: [3, 1] },
            { home: "Germany", away: "France", score: [2, 2] }
        ]);
        expect(scoreboard.toString()).equal(
`Uruguay 6 - Italy 6
Spain 10 - Brazil 2
Mexico 0 - Canada 5
Argentina 3 - Australia 1
Germany 2 - France 2`);
    });

});
