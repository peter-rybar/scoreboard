export type Score = [home: number, away: number];

export interface GameScore {
    home: string;
    away: string;
    score: Score;
}

export class Scoreboard {
    private _gameScores: GameScore[] = [];

    /**
     * Start a new game, assuming initial score 0 – 0 and adding it the scoreboard.
     */
    startGame(home: string, away: string) {
        const match = this._gameScores.find(
            s => s.home == home && s.away == away);
        if (match) {
            throw new Error("Game aleready exists");
        }
        this._gameScores.push({
            home,
            away,
            score: [0, 0]
        });
    }

    /**
     * Update score.
     */
    updateGameScore(home: string, away: string, score: Score) {
        const match = this._gameScores.find(
            s => s.home == home && s.away == away);
        if (match) {
            match.score = score;
        } else {
            throw new Error("No such game");
        }
    }

    /**
     * Finish game currently in progress. This removes a match from the scoreboard.
     */
    finishGame(home: string, away: string) {
        const match = this._gameScores.find(
            s => s.home == home && s.away == away);
        if (!match) {
            throw new Error("No such game");
        }

        this._gameScores = this._gameScores.filter(
            s => s.home !== home && s.away !== away);
    }

    /**
     * Get a summary of games in progress ordered by their total score. The games with the same
     * total score will be returned ordered by the most recently started match in the scoreboard.
     * @returns
     */
    summary() {
        const ordered = [...this._gameScores]
            .map((s, i) => ({...s, total: s.score[0] + s.score[1], i }))
            .sort((s1, s2) => {
                if ( s1.total < s2.total ){
                    return 1;
                }
                if ( s1.total > s2.total ){
                    return -1;
                }
                if ( s1.total === s2.total){
                    if ( s1.i < s2.i ){
                        return 1;
                    }
                    if ( s1.i > s2.i ){
                        return -1;
                    }
                }
                return 0;
            })
            .map(s => ({
                home: s.home,
                away: s.away,
                score: s.score
            }));
        return ordered;
    }

    toString() {
        return this.summary()
            .map(s => `${s.home} ${s.score[0]} - ${s.away} ${s.score[1]}`)
            .join("\n");
    }
}
