# Scoreboard

Live Football World Cup Scoreboard library and frontend application demo.

Live demo <https://peter-rybar.gitlab.io/scoreboard/>

## Requirements

You are working in a sports data company, and we would like you to develop a new Live Football
World Cup Scoreboard library (or frontend application) that shows all the ongoing matches and their
scores.

The scoreboard supports the following operations:

1. Start a new game, assuming initial score 0 – 0 and adding it the scoreboard.
This should capture following parameters:
    - Home team
    - Away team
2. Update score. This should receive a pair of absolute scores: home team score and away
team score.
3. Finish game currently in progress. This removes a match from the scoreboard.
4. Get a summary of games in progress ordered by their total score. The games with the same
total score will be returned ordered by the most recently started match in the scoreboard.

For example, if following matches are started in the specified order and their scores
respectively updated:

```text
a. Mexico 0 - Canada 5
b. Spain 10 - Brazil 2
c. Germany 2 - France 2
d. Uruguay 6 - Italy 6
e. Argentina 3 - Australia 1
```

The summary should be as follows:

```text
1. Uruguay 6 - Italy 6
2. Spain 10 - Brazil 2
3. Mexico 0 - Canada 5
4. Argentina 3 - Australia 1
5. Germany 2 - France 2
```

## Development

Install dependences

```sh
npm install
```

Run scoreboard service development mode

```sh
npm run test:unit:watch
```

Run frontend development mode

```sh
npm start
```

Create production distribution

```sh
npm run dist
```
